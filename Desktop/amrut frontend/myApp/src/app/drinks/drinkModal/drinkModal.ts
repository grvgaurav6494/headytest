import { Component, Input } from '@angular/core';
import { NavParams, ModalController } from '@ionic/angular';

@Component({
  selector: 'drinkModal',
  templateUrl: './drinkModal.page.html',
  styleUrls: ['../drinks.page.scss'],
})
export class drinkModal {

  @Input() data: any;
  slideOpts = {
    initialSlide: 1,
    speed: 400,
    slidesPerView: 3
  };
  products=[]
  selected=0
  mlList=[]
  mlSelect=0
  quantity=1
  constructor(private navParams: NavParams,public modalController: ModalController) {
    this.products=this.navParams.get("data").product
  }

  ngOnInit(){
    
  }

  selectProduct(pos,product){
    this.selected=pos
    this.mlSelect=0
    this.mlList=product.product
  }

  addToCart(){
    let items=JSON.parse(localStorage.getItem('cart'))==null?[]:JSON.parse(localStorage.getItem('cart'))
    items.push({
      quantity:this.quantity,
        name:this.data.name+" "+this.products[this.selected].name,
        ml:this.products[this.selected].product[this.mlSelect].ml,
        price:this.products[this.selected].product[this.mlSelect].price
    })
    localStorage.setItem('cart',JSON.stringify(items))
    this.modalController.dismiss({
      'dismissed': true
    });
  }
}