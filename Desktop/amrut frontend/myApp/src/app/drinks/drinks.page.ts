import { Component, OnInit } from '@angular/core';
// import * as menu from '.'
import { HttpClient } from '@angular/common/http';
import { ModalController } from '@ionic/angular';
import { drinkModal } from './drinkModal/drinkModal';
import { listModal } from '../listModal/listModal';

@Component({
  selector: 'app-drinks',
  templateUrl: './drinks.page.html',
  styleUrls: ['./drinks.page.scss'],
})
export class DrinksPage implements OnInit {

  menu=[]
  cart=[]
  selectedDrink:String
  filterMenu=[]
  constructor(private httpClient: HttpClient,public modalController: ModalController) { }

  ngOnInit() {
    this.cart=JSON.parse(localStorage.getItem('cart'))==null?[]:JSON.parse(localStorage.getItem('cart'))
    this.httpClient.get("assets/menu.json").subscribe((data:any) =>{
      this.menu = data['drinks'];
      this.getFilterMenu("beer")
    })
  }

  ngOnChanges(){
    this.cart=JSON.parse(localStorage.getItem('cart'))
  }

  async select(item){
    const modal = await this.modalController.create({
      component: drinkModal,
      cssClass: 'my-custom-class',
      componentProps:{data:item},
      swipeToClose: true,
    });
    modal.onWillDismiss().then(data=>{
      this.cart=JSON.parse(localStorage.getItem('cart'))==null?[]:JSON.parse(localStorage.getItem('cart'))
    });
    return await modal.present();

    
  }

  async cartView(){
    const modal = await this.modalController.create({
      component: listModal,
      cssClass: 'my-custom-class',
      swipeToClose: true
    });
    return await modal.present();
  }

  getFilterMenu(catNum){
    this.selectedDrink=catNum
    this.filterMenu=[]
    for(var x=0;x<this.menu.length;x++){
      if(this.menu[x].type==this.selectedDrink){
        this.filterMenu.push(this.menu[x])
      }
    }
  }

}
