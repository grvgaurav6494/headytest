import { Injectable } from '@angular/core';
export const WS_ENDPOINT = "wss://s0txmnw6n9.execute-api.ap-south-1.amazonaws.com/dev";

@Injectable({
  providedIn: 'root'
})
export class SocketService {

  webSocket:WebSocket
  connectSocket(){
    this.webSocket=new WebSocket(WS_ENDPOINT)
    this.webSocket.onopen=(event)=>{
      console.log("Websocket connected!",event)
    }
    this.webSocket.onmessage=(event)=>{
      let message=JSON.parse(event.data)
      console.log('message gaya',message)
    }
    this.webSocket.onclose=(event)=>{
      console.log("Websocket closed!",event)
    }
  }

  sendHotelOrder(item){
    this.webSocket.send(JSON.stringify({action:"sendOrder",data:JSON.stringify(item)}))
  }

  closeConnection(){
    this.webSocket.close();
  }
}