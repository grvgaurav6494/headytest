import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  baseurl="https://dba6ms5ofh.execute-api.ap-south-1.amazonaws.com/dev/"
  constructor(private httpClient: HttpClient) { }

  posts(url,json){
    return this.httpClient.post(this.baseurl+url,json);
  }

  get(url){
    return this.httpClient.get(url);
  }
}
