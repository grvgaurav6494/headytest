import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  addressForm:FormGroup
  @ViewChild('mobile') mobile
  @ViewChild('flat') flat
  @ViewChild('landmark') landmark
  constructor(private fb:FormBuilder,private router: Router) { 
    this.addressForm=fb.group({
      mobile:["",Validators.required],
      flat:["",Validators.required],
      landmark:["",Validators.required]
    })
  }

  ngOnInit() {
  // debugger
 
  }
  
  ionViewDidEnter(){
    debugger
    // this.mobile.setFocus();
  }

  address(){
    debugger
    localStorage.setItem('user',JSON.stringify(this.addressForm.getRawValue()))
    this.router.navigate(['/payment/paymentype/card'])
  }

}
