import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TabsPage } from './tabs.page';

const routes: Routes = [
  {
    path: 'tabs',
    component: TabsPage,
    children: [
      {
        path: 'drinks',
        loadChildren: () => import('../drinks/drinks.module').then(m => m.DrinksPageModule)
      },
      {
        path: 'maincourse',
        loadChildren: () => import('../maincourse/maincourse.module').then( m => m.MaincoursePageModule)
      },
      {
        path: 'starter',
        loadChildren: () => import('../starter/starter.module').then( m => m.StarterPageModule)
      },
      {
        path: '',
        redirectTo: '/tabs/drinks',
        pathMatch: 'full'
      }
    ]
  },
  {
    path: '',
    redirectTo: '/tabs/drinks',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TabsPageRoutingModule {}
