import { Component, OnInit } from '@angular/core';
import { WebIntent } from '@ionic-native/web-intent/ngx';

@Component({
  selector: 'app-upi',
  templateUrl: './upi.page.html',
  styleUrls: ['./upi.page.scss'],
})
export class UpiPage implements OnInit {

  pa="7506936046@oksbi"
  pn="Gaurav Shetty"
  tid="dsdafsaf"
  tr="98"
  am="2"
  cu="INR"
  tn="app payment"
  constructor(private webIntent: WebIntent) { }

  ngOnInit() {
  }

  // pay(){
  //   let options={
  //     action: this.webIntent.ACTION_VIEW,
  //     url:"upi://pay?pa="+this.pa+"&pn="+this.pn+"&tid="+this.tid+"&tr="+this.tr+"&am="+this.am+"&cu="+this.cu+"&tn="+this.tn
  //   }
  //   this.webIntent.startActivity(options).then(onSuccess=>{
  //     debugger
  //   }, onError=>{
  //     debugger
  //   });  
  // }

  pay(){
    const options = {
      action: this.webIntent.ACTION_VIEW,
      url: 'upi://pay?pa=7506936046@a&pn=Gaurav&tr=213&am=12&cu=INR&tn=demopay',
      type: 'application/vnd.android.package-archive'
    }
    
    this.webIntent.startActivity(options).then(onSuccess=>{
      debugger
    }, onError=>{
      debugger
    });
  }

}
