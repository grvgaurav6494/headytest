import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { UpiPage } from './upi.page';

describe('UpiPage', () => {
  let component: UpiPage;
  let fixture: ComponentFixture<UpiPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpiPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(UpiPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
