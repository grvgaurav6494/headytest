import { Component, OnInit, OnDestroy } from '@angular/core';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { DataService } from 'src/app/service/data.service';
import { Router } from '@angular/router';
import { isEmptyExpression } from '@angular/compiler';
import { webSocket } from "rxjs/webSocket";
import { SocketService } from '../../service/socket.service'

@Component({
  selector: 'app-card',
  templateUrl: './card.page.html',
  styleUrls: ['./card.page.scss'],
})
export class CardPage implements OnInit {

  constructor(private iab: InAppBrowser,private data:DataService,private route:Router,private socketService:SocketService) { }

  ngOnInit() {
    this.socketService.connectSocket();
  }

  loadUi(){
    const browser = this.iab.create('https://payments-test.cashfree.com/order/#JnJgaNs1WG3tOxmjRqRj');

    // browser.executeScript(...);

    // browser.insertCSS(...);
    // browser.on('loadstop').subscribe(event => {
    //   browser.insertCSS({ code: "body{color: red;" });
    // });
  }

  creatOrder(){
    let cart=JSON.parse(localStorage.getItem('cart'))
    let user=JSON.parse(localStorage.getItem('user'))
    let load={
      items:cart,
      user:user
    }
    debugger
    this.data.posts('insertorder',load).subscribe((val:any)=>{
      if(Object.keys(val).length==0){
        this.socketService.sendHotelOrder(load)
        this.socketService.closeConnection();
        this.route.navigate(['orderstatus'])
      }
    })
  }

  sendOrder(item){
    // let ws = new WebSocket("wss://s0txmnw6n9.execute-api.ap-south-1.amazonaws.com/dev");
    // const subject = webSocket("wss://s0txmnw6n9.execute-api.ap-south-1.amazonaws.com/dev");
    
  }

}
