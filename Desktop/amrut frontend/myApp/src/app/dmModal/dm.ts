import { Component, Input } from '@angular/core';
import { NavParams, ModalController } from '@ionic/angular';

@Component({
  selector: 'dmModal',
  templateUrl: './dm.page.html',
  styleUrls: ['./dm.page.scss'],
})
export class dmModal {

  item=[]
  constructor(private navParams: NavParams){
    this.item=this.navParams.get("data")
  }
}