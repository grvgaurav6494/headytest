import { Component, Input } from '@angular/core';
import { NavParams, ModalController } from '@ionic/angular';

@Component({
  selector: 'listModal',
  templateUrl: './listModal.page.html',
  styleUrls: ['./list.page.scss'],
})
export class listModal {

    cart=[]
    calcSum=0
    constructor(public modalController: ModalController){}
    ngOnInit(){
        this.cart=JSON.parse(localStorage.getItem('cart'))
        for(var x=0;x<this.cart.length;x++){
            this.calcSum=this.calcSum+(this.cart[x].quantity*this.cart[x].price)
        }
    }

    dismissModal(){
        this.modalController.dismiss({
            'dismissed': true
          });
    }
}