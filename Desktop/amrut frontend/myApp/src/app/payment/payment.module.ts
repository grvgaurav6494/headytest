import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PaymentPageRoutingModule } from './payment-routing.module';

import { PaymentPage } from './payment.page';
import { CardPage } from '../paymentPages/card/card.page';
import { UpiPage } from '../paymentPages/upi/upi.page';
import { WebIntent } from '@ionic-native/web-intent/ngx';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PaymentPageRoutingModule
  ],
  declarations: [PaymentPage],
  providers:[WebIntent]
})
export class PaymentPageModule {}
