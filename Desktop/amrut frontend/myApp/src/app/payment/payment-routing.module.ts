import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PaymentPage } from './payment.page';

const routes: Routes = [
  {
    path: 'paymentype',
    component: PaymentPage,
    children: [
  {
    path: 'card',
    loadChildren: () => import('../paymentPages/card/card.module').then( m => m.CardPageModule)
  },
  {
    path: 'upi',
    loadChildren: () => import('../paymentPages/upi/upi.module').then( m => m.UpiPageModule)
  },
  {
    path: '/payment',
    redirectTo: '/paymentype/card',
    pathMatch: 'full'
  }
]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PaymentPageRoutingModule {}
