import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ModalController, PickerController } from '@ionic/angular';
import { dmModal } from '../dmModal/dm';
import { PickerOptions } from "@ionic/core";

@Component({
  selector: 'app-starter',
  templateUrl: './starter.page.html',
  styleUrls: ['./starter.page.scss'],
})
export class StarterPage implements OnInit {

  cart=[]
  menu=[]
  selectedStarter:String
  filterMenu=[]
  constructor(private httpClient: HttpClient,public modalController: ModalController,private pickerController: PickerController) { }

  ngOnInit() {
    this.cart=JSON.parse(localStorage.getItem('cart'))==null?[]:JSON.parse(localStorage.getItem('cart'))
    this.httpClient.get("assets/menu.json").subscribe((data:any) =>{
      this.menu = data['starter'];
      this.getFilterMenu("indian")
    })
  }

  async getModal(item){
    const modal = await this.modalController.create({
      component: dmModal,
      cssClass: 'my-custom-class',
      componentProps:{data:item},
      swipeToClose: true
    });
    modal.onWillDismiss().then(data=>{
      this.cart=JSON.parse(localStorage.getItem('cart'))==null?[]:JSON.parse(localStorage.getItem('cart'))
    });
    return await modal.present();
  }

  async getPicker(item){
    let options: PickerOptions = {
      buttons: [
        {
          text: "Cancel",
          role: 'cancel'
        },
        {
          text:'Confirm Quantity',
          handler:(data:any) => {
            let cart=JSON.parse(localStorage.getItem('cart'))==null?[]:JSON.parse(localStorage.getItem('cart'))
            item.quantity=data.data.value
            cart.push(item)
            localStorage.setItem('cart',JSON.stringify(cart))
          }
        }
      ],
      columns:[{
        name:'data',
        options:this.getColumnOptions()
      }]
    };

    let picker = await this.pickerController.create(options);
    picker.present()
  }

  getColumnOptions(){
    let options = [];
    for(var x=1;x<11;x++){
      options.push({text:x,value:x});
    };
    return options;
  }

  getFilterMenu(catNum){
    this.selectedStarter=catNum
    this.filterMenu=[]
    for(var x=0;x<this.menu.length;x++){
      if(this.menu[x].type==this.selectedStarter){
        this.filterMenu.push(this.menu[x])
      }
    }
  }

}
