import { Component, OnInit } from '@angular/core';
import { Platform } from '@ionic/angular';
import { Router } from '@angular/router';
@Component({
  selector: 'app-orderstatus',
  templateUrl: './orderstatus.page.html',
  styleUrls: ['./orderstatus.page.scss'],
})
export class OrderstatusPage implements OnInit {

  constructor(private platform: Platform,private route:Router) {
    this.platform.backButton.subscribeWithPriority(10, () => {
      console.log('Handler was called!');
      this.route.navigate(["/tabs/drinks"])
    });
  }

  ngOnInit() {
  }

}
