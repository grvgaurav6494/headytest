var mongoose = require("mongoose");


var dimensionSchema = mongoose.Schema({
	size: String,
	gsm: [
            { 
	            thick: String, 
	            qty: String, 
	            totalOrder: String
	        }
        ]
    // paper: { type: mongoose.Schema.Types.ObjectId, ref: 'TestPaper' }

})

module.exports = mongoose.model('Dimension', dimensionSchema);
