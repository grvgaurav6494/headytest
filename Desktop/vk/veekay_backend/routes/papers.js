const express = require("express");
const router = express.Router();
const Paper = require("../models/papers");
const authController = require('../controllers/auth');
const Test = require('../models/tests');
const TestPaper = require('../models/paper2');
const Dimension = require('../models/dimension');


let paperType = ["MAPLITHO", "ART PAPER", "ART CARD", "ALBASTER"];

let dimensions = [
    {
        size:"22*28", 
        gsm:[
            {thick:"60",qty:"300",totalOrder: "0"},
            {thick:"70",qty:"300",totalOrder: "0"},
            {thick:"80",qty:"300",totalOrder: "0"},
            {thick:"90",qty:"300",totalOrder: "0"},
            {thick:"100",qty:"300",totalOrder: "0"},
            {thick:"170",qty:"300",totalOrder: "0"},
            {thick:"210",qty:"300",totalOrder: "0"},
            {thick:"240",qty:"300",totalOrder: "0"},
            {thick:"250",qty:"300",totalOrder: "0"},
            {thick:"300",qty:"300",totalOrder: "0"},
            {thick:"350",qty:"300",totalOrder: "0"},
            {thick:"400",qty:"300",totalOrder: "0"},
            {thick:"450",qty:"300",totalOrder: "0"},
        ]
    },
    {
        size:"23*36", 
        gsm:[
            {thick:"60",qty:"300",totalOrder: "0"},
            {thick:"70",qty:"300",totalOrder: "0"},
            {thick:"80",qty:"300",totalOrder: "0"},
            {thick:"90",qty:"300",totalOrder: "0"},
            {thick:"100",qty:"300",totalOrder: "0"},
            {thick:"170",qty:"300",totalOrder: "0"},
            {thick:"210",qty:"300",totalOrder: "0"},
            {thick:"240",qty:"300",totalOrder: "0"},
            {thick:"250",qty:"300",totalOrder: "0"},
            {thick:"300",qty:"300",totalOrder: "0"},
            {thick:"350",qty:"300",totalOrder: "0"},
            {thick:"400",qty:"300",totalOrder: "0"},
            {thick:"450",qty:"300",totalOrder: "0"},
        ]
    },
    {
        size:"24*34", 
        gsm:[
            {thick:"60",qty:"300",totalOrder: "0"},
            {thick:"70",qty:"300",totalOrder: "0"},
            {thick:"80",qty:"300",totalOrder: "0"},
            {thick:"90",qty:"300",totalOrder: "0"},
            {thick:"100",qty:"300",totalOrder: "0"},
            {thick:"170",qty:"300",totalOrder: "0"},
            {thick:"210",qty:"300",totalOrder: "0"},
            {thick:"240",qty:"300",totalOrder: "0"},
            {thick:"250",qty:"300",totalOrder: "0"},
            {thick:"300",qty:"300",totalOrder: "0"},
            {thick:"350",qty:"300",totalOrder: "0"},
            {thick:"400",qty:"300",totalOrder: "0"},
            {thick:"450",qty:"300",totalOrder: "0"},
        ]
    },
    {
        size:"25*36", 
        gsm:[
            {thick:"60",qty:"300",totalOrder: "0"},
            {thick:"70",qty:"300",totalOrder: "0"},
            {thick:"80",qty:"300",totalOrder: "0"},
            {thick:"90",qty:"300",totalOrder: "0"},
            {thick:"100",qty:"300",totalOrder: "0"},
            {thick:"170",qty:"300",totalOrder: "0"},
            {thick:"210",qty:"300",totalOrder: "0"},
            {thick:"240",qty:"300",totalOrder: "0"},
            {thick:"250",qty:"300",totalOrder: "0"},
            {thick:"300",qty:"300",totalOrder: "0"},
            {thick:"350",qty:"300",totalOrder: "0"},
            {thick:"400",qty:"300",totalOrder: "0"},
            {thick:"450",qty:"300",totalOrder: "0"},
        ]
    },
    {
        size:"28*40", 
        gsm:[
            {thick:"60",qty:"300",totalOrder: "0"},
            {thick:"70",qty:"300",totalOrder: "0"},
            {thick:"80",qty:"300",totalOrder: "0"},
            {thick:"90",qty:"300",totalOrder: "0"},
            {thick:"100",qty:"300",totalOrder: "0"},
            {thick:"170",qty:"300",totalOrder: "0"},
            {thick:"210",qty:"300",totalOrder: "0"},
            {thick:"240",qty:"300",totalOrder: "0"},
            {thick:"250",qty:"300",totalOrder: "0"},
            {thick:"300",qty:"300",totalOrder: "0"},
            {thick:"350",qty:"300",totalOrder: "0"},
            {thick:"400",qty:"300",totalOrder: "0"},
            {thick:"450",qty:"300",totalOrder: "0"},
        ]
    },
    {
        size:"30*40", 
        gsm:[
            {thick:"60",qty:"300",totalOrder: "0"},
            {thick:"70",qty:"300",totalOrder: "0"},
            {thick:"80",qty:"300",totalOrder: "0"},
            {thick:"90",qty:"300",totalOrder: "0"},
            {thick:"100",qty:"300",totalOrder: "0"},
            {thick:"170",qty:"300",totalOrder: "0"},
            {thick:"210",qty:"300",totalOrder: "0"},
            {thick:"240",qty:"300",totalOrder: "0"},
            {thick:"250",qty:"300",totalOrder: "0"},
            {thick:"300",qty:"300",totalOrder: "0"},
            {thick:"350",qty:"300",totalOrder: "0"},
            {thick:"400",qty:"300",totalOrder: "0"},
            {thick:"450",qty:"300",totalOrder: "0"},
        ]
    },
    {
        size:"36*46", 
        gsm:[
            {thick:"60",qty:"300",totalOrder: "0"},
            {thick:"70",qty:"300",totalOrder: "0"},
            {thick:"80",qty:"300",totalOrder: "0"},
            {thick:"90",qty:"300",totalOrder: "0"},
            {thick:"100",qty:"300",totalOrder: "0"},
            {thick:"170",qty:"300",totalOrder: "0"},
            {thick:"210",qty:"300",totalOrder: "0"},
            {thick:"240",qty:"300",totalOrder: "0"},
            {thick:"250",qty:"300",totalOrder: "0"},
            {thick:"300",qty:"300",totalOrder: "0"},
            {thick:"350",qty:"300",totalOrder: "0"},
            {thick:"400",qty:"300",totalOrder: "0"},
            {thick:"450",qty:"300",totalOrder: "0"},
        ]
    }

];


function paperSeeds() {
    let testPaper = new TestPaper({
        paperType: "Maplitho"
    });
    testPaper.save()
    .then(savedPaper => {
        TestPaper.findById({"_id": savedPaper._id})
        .then(currentPaper => {
            for (var i = 0; i < dimensions.length; i++) {
                let dimension = new Dimension(dimensions[i]);
                // dimension.paper = currentPaper._id;
                dimension.save()
                .then(savedDimension => {
                    currentPaper.dimension.push(savedDimension._id)
                    currentPaper.save()
                    .then(finalPaper => {
                        currentPaper.save()
                        .then(afterCall => {
                            return afterCall;
                        })
                        .catch(err6 => {
                            return err6
                        })
                    })
                    .catch(err3 => {
                        return err3;
                    })
                })
                .catch(err4 => {
                    return err4;
                })
            }
            return;
        })
        .catch(err => {
            return err;
        })
    })
    .catch(err2 => {
        return err2;
    })

    // for (let k = 0; k < paperType.length; k++) {
        

    //     testPaper.paperType = paperType[k];
    //     testPaper.save()
    //     .then(savedPaper => {
    //         console.log(savedPaper._id)
    //         TestPaper.findById({"_id": savedPaper._id})
    //         .then(currentPaper => {
    //             console.log(currentPaper)
    //             for (let m = 0; m < dimensions.length; m++) {
    //                 let dimension = new Dimension();
    //                 dimension[m].save()
    //                 .then(savedDimension => {
    //                     currentPaper.dimension.push(savedDimension._id)
    //                     currentPaper.save()
    //                     .then(finalPaper => {
    //                        console.log(finalPaper);
    //                     })
    //                     .catch(err => {
    //                         return err;
    //                     })
    //                 })
    //                 .catch(err2 => {
    //                     return err2;
    //                 })

    //             }
    //         })
    //         .catch(err4 => {
    //             return err4;
    //         })
    //     })
    //     .catch(err3 => {
    //         return err3;
    //     })
    // }

    // return;
}


let sizeArray = [0, 1, 2, 3, 4, 5, 6, 7]
let gsmArray = [0, 1, 2, 3, 4, 5, 6 , 7, 8, 9, 10, 11, 12, 13]
let i;
let j;


function selectSizeAndGsm(size, gsm) {
    console.log(size)
    switch(size) {
        case "22*28": i = 0; break;
        
        case "23*36": i = 1; break;
        
        case "24*34": i = 2; break;
        
        case "25*36": i = 3; break;
        
        case "28*40": i = 4; break;
        
        case "30*40": i = 5; break;
        
        case "36*46": i = 6; break;
        
        default: console.log("Wrong selection of size")
    }

    switch(gsm) {
        case "60": j = 0; break;
        
        case "70": j = 1; break;
        
        case "80": j = 2; break;
        
        case "90": j = 3; break;
        
        case "100": j = 4; break;
        
        case "170": j = 5; break;
        
        case "210": j = 6; break;

        case "250": j = 7; break;

        case "210": j = 8; break;

        case "300": j = 9; break;

        case "350": j = 10; break;

        case "400": j = 11; break;

        case "450": j = 12; break;
        
        default: console.log("Wrong selection of gsm")
    }

    return {size:i, gsm:j};
}

router.post("/addNewPapersType", (req, res) => {
    // let add = paperSeeds();
    // res.json(add);

    // let testPaper = new TestPaper({
    //     paperType:"Art Card",
    // })

    // testPaper.save()
    // .then(papers => {
    //     res.json(papers);
    // })
    // let testPaper = new TestPaper();

    // TestPaper.findById({"_id": "5c72d4a2762e1110dc9647f5"})
    // .then(paper => {
    //     let dimension1 = new Dimension(req.body);
    //     dimension1.paper = paper._id;
    //     dimension1.save()
    //     .then(savedDimension => {
    //         paper.dimension.push(savedDimension._id)
    //         paper.save()
    //         .then(finalPaper => {
    //             res.json(finalPaper);
    //         })
    //         .catch(err => {
    //             res.json(err);
    //         })
    //     })
    //     .catch(err2 => {
    //         res.json(err2);
    //     })
    // })
    // .catch(err3 => {
    //     res.json(err3);
    // })

    
})

router.get("/getpapers", (req, res) => {
    TestPaper.find({}, {"_v":0, "dimension":"0"})
    .then(papers => {
        res.json(papers);
    })
    .catch(err => {
        res.json(err);
    })
})

router.put("/updatepaper/:id", (req, res) => {
    // let id = req.params
    let submittedPaper = req.body;

    let values = selectSizeAndGsm(submittedPaper.size, submittedPaper.gsm);
    let i = values.size;
    let j = values.gsm;

    TestPaper.find({"_id": submittedPaper.id})
    .then(paper => {
        let dimensionId = paper[0].dimension[i];
        Dimension.find({"_id": dimensionId})
        .then(result => {
            result[0].gsm[j].totalOrder = JSON.parse(result[0].gsm[j].totalOrder) + submittedPaper.count
            result[0].save()
            .then(finalPaper => {
                res.json(finalPaper);
            })
            .catch(err2 => {
                res.json(err2)
            })
        })
        .catch(err => {
            res.json(err);
        })
    })
      
})

router.get("/getpapers/:id", (req, res) => {
    let id = req.params
    TestPaper.findById({"_id": id})
    .then(papers => {
        res.json(paper.dimension);
    })
    .catch(err => {
        res.json(err);
    })
})

router.get("/getPapersType", (req, res) => {
    Test.find({})
    .then(papers => {
        res.json(papers.map(paper => {
            return paper.paperType;
        }));
    })
    .catch(err => {
        res.json(err);
    })
})

router.post("/addPaperType", (req, res) => {
    console.log(req.body);
    Test.create(req.body)
    .then(papers => {
        Test.find({})
        .then(papers => {
            res.json(papers.map(paper => {
                return paper.paperType;
            }));
        })
    })
    .catch(err => {
        console.log(err);
    })
})



// let paperTypes = ["Maplitho", "Hard paper", "Art Card", "Albaster","Special paper"];

// router.get("/savepapers", (req, res) => {
//     let papers = [];
//     for(let j =0; j< paperTypes.length; j++) {
//         papers.push({paper:paperTypes[j], dimension:[]});
//         for(let i = 0; i < papersData.length; i++) {
//             papers[j].dimension.push(papersData[i]);
//         }        
//     }
//     console.log(papers);

//     Paper.insertMany(papers)
//     .then(res => {
//         res.status(200).json(res);
//     })
//     .catch(err => {
//         console.log(err);
//         res.status(501).json(err);
//     }) 
// });

// let papers = [
//     {
//         paper:"Maplitho",
//         quantity:600,
//         totalOrder:0
//     },
//     {
//         paper:"Hard Paper",
//         quantity:600,
//         totalOrder:0
//     },
//     {
//         paper:"Albaster",
//         quantity:600,
//         totalOrder:0
//     },
//     {
//         paper:"Art Card",
//         quantity:600,
//         totalOrder:0
//     },
//     {
//         paper:"Special paper",
//         quantity:600,
//         totalOrder:0
//     }

// ]

// router.get("/addData", (req, res) => {
//     papers.forEach(element => {
//         Paper.create(element)
//         .then(res => {
//             console.log(res);
//         })
//         .catch(err => {
//             console.log(err);
//         })
//     });
// })


router.get("/getPapersData", authController.verifyToken, (req, res) => {
    Paper.find({})
    .then((pro) => {
		res.status(200).json(pro);
	})
	.catch(err => {
    	res.status(400).json({success:false, message:"Jobs not found"});
	});
});

router.post("/updateQuantity/:id", authController.verifyToken, (req, res) => {
    Paper.findByIdAndUpdate({ _id: req.params.id }, { $inc: { quantity: req.body.quantity } }, { new: true })
    .then(() => {
        res.status(200).json("Updated successfully!!");
    })
    .catch(err => {
        res.status(400).json({success:false, message:"Jobs not found"});
    });
})

module.exports = router;